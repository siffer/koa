// 1. 导入koa-router
const Router = require("koa-router");

// 中间件
const { auth } = require("../middleware/auth.middleware");
const { validator, verifyGoods } = require("../middleware/cart.middleware");

// 控件器
const {
  add,
  findAll,
  update,
  remove,
  selectAll,
  unSelectAll,
} = require("../controller/cart.controller");

// 2. 实例化router对象
const router = new Router({ prefix: "/carts" });

// 3. 编写路由规则

// 3.1 添加到购物车接口: 登录, 格式
router.post("/", auth, validator({ goods_id: "number" }), verifyGoods, add);

// 3.2 获取购物车列表
router.get("/", findAll);

// 3.3 更新购物车
router.patch(
  "/:id",
  auth,
  validator({
    number: { type: "number", require: false },
    selected: { type: "bool", require: false },
  }),
  update
);

// 3.4 删除购物车
router.delete("/", auth, validator({ ids: "array" }), remove);

// 3.5 全选
router.post("/selectAll", auth, selectAll);

// 3.6 全不选
router.post("/unSelectAll", auth, unSelectAll);

// 4. 导出router对象
module.exports = router;
